package com.example.demo;

import com.example.demo.domain.Topic;
import com.example.demo.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;
   /*List<Topic> var =new ArrayList<>(Arrays.asList(
            new Topic("1","Alekhya","Good"),
			new Topic("2", "Akshara","bad"),
            new Topic("3","Damu","bad"),
            new Topic("4","jyothi","ok")));*/

   public List<Topic> getalltopics()
   {
       //return var;
       List<Topic> topics = new ArrayList<>();
       topicRepository.findAll().forEach(topics::add); //refer javabrains tutorial on java8 lambda expressions
       return topics;

   }
   public Topic gettopic(String id)
   {
     // return var.stream().filter(t -> t.getId().equals(id)).findFirst().get();

       return topicRepository.findById(id).orElse(null);
   }

    public void addTopic(Topic topic) {

       //var.add(topic);

        topicRepository.save(topic);
    }

    public void deleteTopic(String id) {
/*       Topic topic;
       topic = var.stream().filter(t -> t.getId().equals(id)).findFirst().get();
       var.remove(topic);*/

      topicRepository.deleteById(id);

    }

    public void updateTopic(String id, Topic topic) {

/*       for(int i=0;i<var.size();i++)
       {
          Topic t= var.get(i);
          if(t.getId().equals(id))
          {
              var.set(i,topic);
              return;
          }
       }*/

       topicRepository.save(topic);
    }
}
