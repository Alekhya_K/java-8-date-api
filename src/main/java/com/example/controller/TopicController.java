package com.example.controller;

import com.example.demo.TopicService;
import com.example.demo.domain.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TopicController {
	@Autowired
	private TopicService topicService;

	//GET
	@RequestMapping("/topics")
	public List<Topic> getAllTopics(){
		return topicService.getalltopics();
	}

	//GET
	@RequestMapping("/topics/{id}")
	public Topic getTopic(@PathVariable String id)
	{
		return topicService.gettopic(id);
	}

	//POST(create)
	@RequestMapping(method=RequestMethod.POST, value="/topics")
	public void addTopic(@RequestBody Topic topic)
	{
		topicService.addTopic(topic);
	}

	//PUT(update)
	@RequestMapping(method = RequestMethod.PUT, value = "/topics/{id}")
	public void updateTopic(@PathVariable String id, @RequestBody Topic topic){
		topicService.updateTopic(id,topic);
	}

	//DELETE(delete)
	@RequestMapping(method = RequestMethod.DELETE, value = "/topics/{id}")
	public void deleteTopic(@PathVariable String id)
	{
		topicService.deleteTopic(id);
	}


}
