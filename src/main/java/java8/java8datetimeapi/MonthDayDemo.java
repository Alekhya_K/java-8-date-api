package java8.java8datetimeapi;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.MonthDay;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.ValueRange;

public class MonthDayDemo {

    public static void main(String[] args) {
        MonthDay monthDay = MonthDay.now();
        System.out.println(monthDay);
        LocalDate date = monthDay.atYear(2019);
        System.out.println(date);

        //Java MonthDay class Example: isValidYear()
        System.out.println(monthDay.isValidYear(2020));

        //Java MonthDay class Example: range()

        MonthDay month = MonthDay.now();
        ValueRange r1 = month.range(ChronoField.MONTH_OF_YEAR);
        System.out.println(r1);
        ValueRange r2 = month.range(ChronoField.DAY_OF_MONTH);
        System.out.println(r2);

        //Java OffsetTime class : getHour() , getMinute() ,getSecond()
        //Java OffsetDateTime class  getDayOfMonth(),getDayOfYear(),getDayOfWeek(), toLocalDate()


    }
}
